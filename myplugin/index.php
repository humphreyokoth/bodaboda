<?php
/*

/** 
 * @package Myplugin
*/
/*
Plugin Name: myplugin
Description:This is my custom plugin.
Version:1.0.0
Author:Okoth humphrey
Author URI:http://wordpress.org/plugins/hello-dolly/
License:MIT
Text Domain: myplugin
 */


 // Custom post type bodabodas.
 function bodabodas_custom_post_type(){
	$labels = array(
		'name'=> 'Bodabodas',
		'singular'=> 'bodabodas',
		'add_new'=> 'Add Item',
		'add_new_item'=>'Add Item',
		'edit_item'=>'Edit Item',
		'new_item'=>'New Item',
		'view_item'=>'View Item',
		'search_item'=>'Search Bodabodas',
		'not_found'=> 'No items found',
		'not_found_in_trash'=>'No items found in trash',
		'parent_item_colon'=>'Parent Item'


	);
$args= array(

	'labels'=>$labels,
	'public'=> true,
	'has_archive'=>true,
	'publicly_queryable'=>true,
	'query_var'=>true,
	'rewrite'=>true,
	'capability_type'=>'post',
	'hierachical'=>false,
	'supports'=>array(
		'title',
		'editor',
		'excerpt',
		'thumbnail',
		'revisions',
	),

	'taxonomies'=>array('category'),
	'menu_position'=>5,
	'exclude_from_search'=>false


);
register_post_type('bodabodas',$args);

}
add_action('init','bodabodas_custom_post_type');



